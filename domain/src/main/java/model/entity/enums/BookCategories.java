package model.entity.enums;

public enum BookCategories {
    HORROR,
    CRIME,
    THRILLER,
    FANTASY,
    HISTORY
}
