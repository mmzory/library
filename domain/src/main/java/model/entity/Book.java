package model.entity;

import model.entity.enums.BookCategories;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Entity
@Table(name = "book")
public class Book {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_book")
    private Long idBook;

    @Column(name = "borrowed")
    private boolean borrowed;

    @Column(name = "category")
    @Enumerated(EnumType.STRING)
    private BookCategories category;

    @Column(name = "isbn")
    private String isbn;

    @Column(name = "pages")
    private int pages;

    @Column(name = "release_date")
    private LocalDate releaseDate;

    @Column(length = 1000)
    private String summary;

    @Column(name = "title")
    private String title;
 
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_author")
    private Author author;

    @OneToMany(mappedBy = "book")
    private List<Borrow> borrow;

    @Transient
    private String borrowerName;

    public Book() {
    }

    public Book(String title, BookCategories category, String isbn, LocalDate releaseDate, int pages, String summary) {
        this.title = title;
        this.category = category;
        this.isbn = isbn;
        this.releaseDate = releaseDate;
        this.pages = pages;
        this.summary = summary;
    }

    public String getBorrowerName() {
        return borrowerName;
    }

    public Long getIdBook() {
        return idBook;
    }

    public Author getAuthor() {
        return author;
    }

    public boolean isBorrowed() {
        return borrowed;
    }

    public BookCategories getCategory() {
        return category;
    }

    public String getIsbn() {
        return isbn;
    }

    public int getPages() {
        return pages;
    }

    public LocalDate getReleaseDate() {
        return releaseDate;
    }

    public String getSummary() {
        return summary;
    }

    public String getTitle() {
        return title;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public void setPages(int pages) {
        this.pages = pages;
    }

    public void setReleaseDate(LocalDate releaseDate) {
        this.releaseDate = releaseDate;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public void setBorrowed(boolean borrowed) {
        this.borrowed = borrowed;
    }

    public void setCategory(BookCategories category) {
        this.category = category;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public void setBorrowerName(String borrowerName) {
        this.borrowerName = borrowerName;
    }
}
