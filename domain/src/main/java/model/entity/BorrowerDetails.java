package model.entity;

import javax.persistence.*;

@Entity
@Table(name = "borrower_details")
public class BorrowerDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_borrower_detail")
    private Long idBorrowerDetail;

    private String address;
    private String email;
    private String phone;

    @OneToOne(mappedBy = "borrowerDetail")
    private Borrower borrower;

    public Long getIdBorrowerDetail() {
        return idBorrowerDetail;
    }

    public String getAddress() {
        return address;
    }

    public String getEmail() {
        return email;
    }

    public String getPhone() {
        return phone;
    }
}
