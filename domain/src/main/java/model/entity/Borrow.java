package model.entity;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "borrow")
public class Borrow {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_borrow")
    private Long idBorrow;

    @Column(name = "id_book", insertable = false, updatable = false)
    private Long idBook;

    @Column(name = "id_borrower", insertable = false, updatable = false)
    private Long idBorrower;

    @Column(name = "rental_date")
    private LocalDate rentalDate;

    @ManyToOne
    @JoinColumn(name = "id_borrower")
    private Borrower borrower;

    @ManyToOne
    @JoinColumn(name = "id_book")
    private Book book;

    public Long getIdBorrow() {
        return idBorrow;
    }

    public Long getIdBook() {
        return idBook;
    }

    public Long getIdBorrower() {
        return idBorrower;
    }

    public LocalDate getRentalDate() {
        return rentalDate;
    }
}
