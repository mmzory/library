package repositories;

import utils.EntityManagerUtil;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.TypedQuery;
import java.lang.reflect.ParameterizedType;
import java.util.List;

public abstract class GenRepository<T, K> {

    protected final Class<T> entityClass;
    protected EntityManager entityManager;

    @SuppressWarnings("unchecked")
    protected GenRepository() {
        ParameterizedType genericSuperclass = (ParameterizedType) getClass().getGenericSuperclass();
        this.entityClass = (Class<T>) genericSuperclass.getActualTypeArguments()[0];
        this.entityManager = EntityManagerUtil.getInstance().getEntityManagerFactory().createEntityManager();
    }

    protected void create(T entity) {
        EntityTransaction transaction = null;
        try {
            transaction = entityManager.getTransaction();
            transaction.begin();
            entityManager.persist(entity);
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
        }
    }

    protected List<T> findAll() {
        TypedQuery<T> query = entityManager.createQuery("select t from " + entityClass.getSimpleName() + " t", entityClass);
        return query.getResultList();
    }

    protected T find(K id) {
        return entityManager.find(entityClass, id);
    }

    protected T findBy(String columnName, String condition) {
        EntityManager entityManager = EntityManagerUtil.getInstance().getEntityManagerFactory().createEntityManager();
        TypedQuery<T> query = entityManager.createQuery("select t from " + entityClass.getSimpleName() + " t where t." + columnName + " = :condition", entityClass);
        query.setParameter("condition", condition);
        List<T> objects = query.getResultList();

        if (objects.isEmpty()) {
            return null;
        }

        return objects.get(0);
    }
}
