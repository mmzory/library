package repositories;

import model.entity.Book;
import utils.EntityManagerUtil;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import java.util.List;

public class BookRepository extends GenRepository<Book, Long> {
    private static BookRepository instance = null;

    public static BookRepository getInstance() {
        if (instance == null) {
            instance = new BookRepository();
        }
        return instance;
    }

    public void addBook(Book book) {
        super.create(book);
    }

    public List<Book> findAllBooks() {
        return super.findAll();
    }

    public Book find(long id) {
        return super.find(id);
    }

    public Book findBookBy(String columnName, String condition) {
        return super.findBy(columnName, condition);
    }
}
