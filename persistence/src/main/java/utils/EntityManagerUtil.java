package utils;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class EntityManagerUtil {
    private static EntityManagerUtil instance = null;
    EntityManagerFactory entityManagerFactory;

    private EntityManagerUtil() {
        entityManagerFactory = Persistence.createEntityManagerFactory("libraryPersistenceUnit");
    }

    public static EntityManagerUtil getInstance() {
        if (instance == null) {
            instance = new EntityManagerUtil();
        }
        return instance;
    }

    public EntityManagerFactory getEntityManagerFactory() {
        return entityManagerFactory;
    }
}
