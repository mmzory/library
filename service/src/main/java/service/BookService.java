package service;

import model.entity.Author;
import model.entity.Book;
import repositories.BookRepository;

import java.util.List;

public class BookService {
    private static BookService instance = null;

    private BookRepository bookRepository;

    private BookService() {
        this.bookRepository = BookRepository.getInstance();
    }

    public static BookService getInstance() {
        if (instance == null) {
            instance = new BookService();
        }
        return instance;
    }

    public List<Book> getBooks() {
        List<Book> books = bookRepository.findAllBooks();
        for (Book book: books) {
            if (book.getAuthor() == null) {
                book.setAuthor(new Author("Unknown", null, null));
            }
            if (!book.isBorrowed()) {
                book.setBorrowerName("-");
            }
        }
        return books;
    }

    public Book getBook(Long id) {
        return bookRepository.find(id);
    }

    public boolean addBook(Book book) {
        if (isBookAlreadyExists(book.getTitle())) {
            return false;
        } else if (isbnIsNotUnique(book.getIsbn())) {
            return false;
        }

        bookRepository.addBook(book);
        return true;
    }

    private boolean isbnIsNotUnique(String isbn) {
        Book book = bookRepository.findBookBy("isbn", isbn);
        return book != null;
    }

    private boolean isBookAlreadyExists(String title) {
        Book book = bookRepository.findBookBy("title", title);
        return book != null;
    }
}
