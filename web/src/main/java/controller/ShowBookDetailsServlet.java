package controller;

import model.entity.Book;
import service.BookService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/ShowBookDetailsServlet")
public class ShowBookDetailsServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (req.getParameter("radio") == null) {
            req.setAttribute("errorMessage", "Database is probably empty. Add any book and try again!");
            req.getRequestDispatcher("subpages/confirmation/error.jsp").forward(req, resp);
        } else {
            BookService bookService = BookService.getInstance();
            Book book = bookService.getBook(Long.parseLong(req.getParameter("radio")));

            req.setAttribute("book", book);
            req.getRequestDispatcher("subpages/showBookDetails.jsp").forward(req, resp);
        }
    }
}
