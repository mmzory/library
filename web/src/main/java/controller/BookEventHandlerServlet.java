package controller;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/BookEventHandlerServlet")
public class BookEventHandlerServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        switch (req.getParameter("action")) {
            case "Add":
                resp.sendRedirect("/AddBookServlet");
                break;
            case "Edit":
                resp.sendRedirect("/EditBookServlet");
                break;
            case "ShowDetails":
                req.getRequestDispatcher("/ShowBookDetailsServlet").forward(req, resp);
                break;
            case "Delete":
                resp.sendRedirect("/DeleteBookServlet");
                break;
            case "SetAuthor":
                resp.sendRedirect("/SetAuthorBookServlet");
                break;
            case "Back":
                resp.sendRedirect("/HomeServlet");
                break;
        }
    }
}
