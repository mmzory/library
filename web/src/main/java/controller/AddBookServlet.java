package controller;

import model.entity.Book;
import model.entity.enums.BookCategories;
import service.BookService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDate;

@WebServlet("/AddBookServlet")
public class AddBookServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setAttribute("categories", BookCategories.values());
        req.setAttribute("currentDate", LocalDate.now());
        req.getRequestDispatcher("subpages/addBook.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (req.getParameter("action").equals("Back")) {
            resp.sendRedirect("/HomeServlet");
            return;
        }

        String title = req.getParameter("title");
        String bookCategory = req.getParameter("category");
        String isbn = req.getParameter("isbn");
        String release = req.getParameter("release");
        String pages = req.getParameter("pages");
        String summary = req.getParameter("summary");

        if (title.isEmpty() || isbn.isEmpty() || release.isEmpty() || pages.isEmpty()) {
            req.setAttribute("errorMessage", "Fill title, isbn, release date and pages and try again!");
            req.getRequestDispatcher("subpages/confirmation/error.jsp").forward(req, resp);
            return;
        }

        try {
            boolean success = addBook(title, BookCategories.valueOf(bookCategory), isbn, LocalDate.parse(release), Integer.parseInt(pages), summary);

            if (success) {
                resp.sendRedirect("/HomeServlet");
            } else {
                req.setAttribute("object", "Book");
                req.setAttribute("name", req.getParameter("title"));
                req.getRequestDispatcher("subpages/confirmation/alreadyExists.jsp").forward(req, resp);
            }
        } catch (Exception e) {
            req.setAttribute("errorMessage", "Incorrect input data! Fill all fields by correct data and try again.");
            req.getRequestDispatcher("subpages/confirmation/error.jsp").forward(req, resp);
        }
    }

    private boolean addBook(String title, BookCategories category, String isbn, LocalDate release, int pages, String summary) {
        return BookService.getInstance().addBook(
                new Book(title, category, isbn, release, pages, summary));
    }
}
