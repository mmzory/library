<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<!doctype html>
<html lang="pl">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta charset="UTF-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
        <title>Library</title>
    </head>

    <body>
        <%@include file="WEB-INF/header.jspf" %>
        <div class="container">
            <form action="${pageContext.request.contextPath}/BookEventHandlerServlet" method="post">
                <table class="table">
                    <thead class="thead-light">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Title</th>
                        <th scope="col">ISBN</th>
                        <th scope="col">Author</th>
                        <th scope="col">Category</th>
                        <th scope="col">Release</th>
                        <th scope="col">Pages</th>
                        <th scope="col">Borrower</th>
                        <th scope="col">&nbsp;</th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:choose>
                        <c:when test="${fn:length(requestScope.books) <= 0}">
                            <tr>
                                <td>
                                    No records in database!
                                </td>
                            </tr>
                        </c:when>
                        <c:otherwise>
                            <c:forEach var="book" items="${requestScope.books}" varStatus="loop">
                                <tr>
                                    <th scope="row">${loop.index + 1}</th>
                                    <td>${book.title}</td>
                                    <td>${book.isbn}</td>
                                    <td>${book.author.firstName} ${book.author.lastName}</td>
                                    <td>${book.category}</td>
                                    <td>${book.releaseDate}</td>
                                    <td>${book.pages}</td>
                                    <td>${book.borrowerName}</td>
                                    <td>
                                        <c:if test="${loop.index == 0}">
                                            <input type="radio" name="radio" id="radio" value="${book.idBook}" checked>
                                        </c:if>
                                        <c:if test="${loop.index > 0}">
                                            <input type="radio" name="radio" id="radio" value="${book.idBook}">
                                        </c:if>
                                    </td>
                                </tr>
                            </c:forEach>
                        </c:otherwise>
                    </c:choose>
                    </tbody>
                </table>

                <div class="d-flex justify-content-around">
                    <input class="btn btn-primary col-2" type="submit" name="action" value="Add">
                    <input class="btn btn-secondary col-2" type="submit" name="action" value="Edit">
                    <input class="btn btn-info col-2" type="submit" name="action" value="ShowDetails">
                    <input class="btn btn-dark col-2" type="submit" name="action" value="Delete">
                </div>
            </form>
        </div>
    </body>
</html>
