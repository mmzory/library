<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!doctype html>
<html lang="pl">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta charset="UTF-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
        <title>Library - Add book</title>
    </head>
    <body>
        <%@include file="../WEB-INF/header.jspf"%>
        <div class="container">
            <form action="/AddBookServlet" method="post">
                <div class="row">
                    <div class="col">
                        <label>Title:</label>
                    </div>
                    <div class="col">
                        <label>Category:</label>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <input class="form-control" type="text" name="title">
                    </div>
                    <div class="col">
                        <select class="col-5 custom-select" name="category">
                            <c:forEach items="${requestScope.categories}" var="category">
                                <option value="${category}" selected>${category}</option>
                            </c:forEach>
                        </select>
                    </div>
                </div><br />
                <div class="row">
                    <div class="col">
                        <label>ISBN:</label>
                    </div>
                    <div class="col">
                        <label>Release:</label>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <input class="form-control" type="text" name="isbn">
                    </div>
                    <div class="col">
                        <input class="form-control" type="date" name="release" value="${requestScope.currentDate}">
                    </div>
                </div><br />
                <div class="row">
                    <div class="col">
                        <label>Pages:</label><br />
                    </div>
                    <div class="col">

                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <input class="form-control" type="text" name="pages">
                    </div>
                    <div class="col">

                    </div>
                </div><br />
                <div class="row">
                    <div class="col">
                        <label>Summary:</label>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <textarea class="form-control" type="text" name="summary"></textarea>
                    </div>
                </div><br />
                <div class="d-flex justify-content-around">
                    <input class="btn btn-success col-2" type="submit" name="action" value="AddBook">
                    <input class="btn btn-success col-2" type="submit" name="action" value="Back">
                </div>
            </form>
        </div>
    </body>
</html>
