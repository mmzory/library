<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!doctype html>
<html lang="pl">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta charset="UTF-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
        <title>Book details</title>
    </head>
    <body>
        <script src="https://unpkg.com/ionicons@4.5.10-0/dist/ionicons.js"></script>
        <%@include file="../WEB-INF/header.jspf"%>
        <div class="container">
            <div class="row">
                <div class="col-4">
                    <table class="table">
                        <tr>
                            <td class="table-light">
                                <ion-icon name="list" size="large" class="align-middle"></ion-icon> Title:
                            </td>
                            <td class="table-light">
                                ${requestScope.book.title}
                            </td>
                        </tr>
                        <tr>
                            <td class="table-light">
                                <ion-icon name="person" size="large" class="align-middle"></ion-icon> Author:
                            </td>
                            <td class="table-light">
                                ${requestScope.book.author.firstName} ${requestScope.book.author.lastName}
                            </td>
                        </tr>
                        <tr>
                            <td class="table-light">
                                <ion-icon name="print" size="large" class="align-middle"></ion-icon> Release:
                            </td>
                            <td class="table-light">
                                ${requestScope.book.releaseDate}
                            </td>
                        </tr>
                        <tr>
                            <td class="table-light">
                                <ion-icon name="information-circle-outline" size="large" class="align-middle"></ion-icon> Category:
                            </td>
                            <td class="table-light">
                                ${requestScope.book.category}
                            </td>
                        </tr>
                        <tr>
                            <td class="table-light">
                                <ion-icon name="pricetags" size="large" class="align-middle"></ion-icon> ISBN:
                            </td>
                            <td class="table-light">
                                ${requestScope.book.isbn}
                            </td>
                        </tr>
                        <tr>
                            <td class="table-light">
                                <ion-icon name="book" size="large" class="align-middle"></ion-icon> Pages:
                            </td>
                            <td class="table-light">
                                ${requestScope.book.pages}
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="col">
                    <table class="table">
                        <tr>
                            <td class="table-light">
                                <div class="text-center">
                                    <div class="custom-control-inline">
                                        <ion-icon name="paper" size="large" class="align-middle">
                                        </ion-icon><h4 class="px-5"> Summary </h4>
                                        <ion-icon name="paper" size="large"></ion-icon>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <p>${requestScope.book.summary}</p>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <form action="/BookEventHandlerServlet" method="post">
                <input type="hidden" name="radio" value="${requestScope.book.idBook}">
                <div class="d-flex justify-content-around">
                    <input class="btn btn-success col-3" type="submit" name="action" value="Edit">
                    <input class="btn btn-dark col-3" type="submit" name="action" value="SetAuthor">
                    <input class="btn btn-info col-3" type="submit" name="action" value="Back">
                </div>
            </form>
        </div>
    </body>
</html>
