<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!doctype html>
<html lang="pl">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta charset="UTF-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
        <title>Error</title>
    </head>
    <body>
        <%@include file="../../WEB-INF/header.jspf"%>
        <div class="container"><br />
            <h5 class="text-center">${requestScope.errorMessage}</h5><br />
            <a href="/HomeServlet"><button class="btn btn-info">Back to main page</button></a>
        </div>
    </body>
</html>